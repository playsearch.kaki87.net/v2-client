const EventSource = (typeof window !== 'undefined' ? window : require('launchdarkly-eventsource')).EventSource;

module.exports = ({ host }) => ({
    search: ({
        query,
        onData
    }) => new Promise((resolve, reject) => {
        try {
            const
                eventSource = new EventSource(`${host}/search?query=${query}`),
                onError = error => {
                    eventSource.removeEventListener('error', onError);
                    eventSource.close();
                    reject(error);
                },
                result = {};
            eventSource.addEventListener('error', onError);
            eventSource.addEventListener('message', ({ data }) => {
                data = JSON.parse(data);
                if(data.status === 'DONE'){
                    eventSource.close();
                    resolve(result);
                }
                else if(data.appId){
                    if(!result[data.appId])
                        result[data.appId] = {};
                    Object.assign(result[data.appId], data);
                    if(onData) onData({
                        data,
                        fullData: result[data.appId],
                        result
                    });
                }
            });
        }
        catch(error){
            reject(error);
        }
    })
});